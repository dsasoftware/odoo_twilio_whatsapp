import base64

from odoo import models, fields, api, _
from odoo.exceptions import Warning
# import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from time import sleep

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    sent_whatsapp = fields.Boolean('Whatsapp Sent',help="", default=False)
    
    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        self.ensure_one()
        self.sent_whatsapp = True
        return super(AccountInvoice,self).invoice_print()    
    
    @api.model
    def get_invoice_jpg(self,report_template):
        pdf = self.env.ref(report_template).sudo().render_qweb_pdf([self.id])[0]
        #pdf = self.env['web'].get_pdf([self.id], report_template)
        ATTACHMENT_NAME = 'Factura whatsapp Nro. '
        ctx = {'convert':'pdf2jpg'}
        attach = self.env['ir.attachment'].with_context(ctx).create({
                                            'name': 'Invoice Whatsapp %s'%ATTACHMENT_NAME,
                                            'type': 'binary',
                                            'datas': base64.encodestring(pdf),
                                            'datas_fname': ATTACHMENT_NAME + '.pdf',
                                            'store_fname': ATTACHMENT_NAME,                                                        
                                            'res_model': 'account.invoice',
                                            'res_id': self.id,
                                            'mimetype': 'application/pdf'
                                        })
        config_obj = self.env['ir.config_parameter'].get_param('web.base.url')
        media_url = config_obj + "/web/attachments/token/"+attach.access_token
        self.env.cr.commit()
        #sleep(10)
        return media_url               
    
    @api.multi
    def action_invoice_whatsapp(self):        
        self.ensure_one()
        media_url = ''        
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        template = self.env.ref('account.email_template_edi_invoice', False)                 
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        res_ids = [x.id for x in self]
        for record in self:
            if record.partner_id.mobile_whatsapp and record.partner_id.enabled_whatsapp:
                default_contact_id = self.env['contact.twiliocontact_base'].search([('to','=',record.partner_id.mobile_whatsapp)])
                if default_contact_id:
                    default_contact_id = default_contact_id[0]
                else:
                    data = {'to':record.partner_id.mobile_whatsapp,'contact_name_contactclass':record.partner_id.name,'image':record.partner_id.image}
                    default_contact_id = self.env['contact.twiliocontact_base'].create(data)                
                if default_contact_id:
                    #pdf = self.env['web'].get_pdf([record.id], 'account.report_invoice')
                    pdf = self.env.ref('account.account_invoices').sudo().render_qweb_pdf([record.id])[0]
                    ATTACHMENT_NAME = 'Factura whatsapp Nro. '
                    ctx = {'convert':'pdf2jpg'}
                    attach = self.env['ir.attachment'].sudo().with_context(ctx).create({
                                                        'name': 'Invoice Whatsapp %s'%ATTACHMENT_NAME,
                                                        'type': 'binary',
                                                        'datas': base64.encodestring(pdf),
                                                        'datas_fname': ATTACHMENT_NAME + '.pdf',
                                                        'store_fname': ATTACHMENT_NAME,                                                        
                                                        'res_model': 'account.invoice',
                                                        'res_id': record.id,
                                                        'mimetype': 'application/pdf'
                                                    })
                    self.env.cr.commit()
                    config_obj = self.env['ir.config_parameter'].get_param('web.base.url')
                    media_url = config_obj + "/web/attachments/token/"+attach.access_token
                    ctx = dict(
                        default_to=self.partner_id.mobile_whatsapp,
                        default_contact_name=default_contact_id.id,
                        default_res_id=record.id,
                        default_body='Factura',
                        mark_invoice_as_sent=True,
                        default_media_url=media_url,
                        whatsapp_sent=True,
                        attachment_ids=[attach.id],
                        custom_layout="account.mail_template_data_notification_email_account_invoice"
                    ) 
                    return {
                        'name': 'Whatsapp Compose',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'twiliosms.base',
                        'target': 'new',
                        'type': 'ir.actions.act_window',
                        'context': ctx
                     }
                else:
                    raise Warning("You must configure the number for whatsapp")
            else:
                raise Warning("Partner has no whatsapp number or does not support messages")
            
    @api.model
    def send_invoice_whatsapp(self):  
        template = self.env.ref('account.email_template_edi_invoice', False)  
        twilio_base = self.env['twiliosms.base']
        twilio_contact = self.env['contact.twiliocontact_base']
        if self.partner_id.mobile_whatsapp and self.partner_id.enabled_whatsapp:
            default_contact_id = twilio_contact.search([('to','=',self.partner_id.mobile_whatsapp)])[0]
            if not default_contact_id:  
                data = {
                        'contact_name_contactclass':self.partner_id.name,
                        'to':self.partner_id.mobile_whatsapp,
                        'image':self.partner_id.mobile_whatsapp.image                     
                    }
                default_contact_id = twilio_contact.create(data)
            media_url = self.get_invoice_jpg('account.account_invoices')
            data = {
                    'contact_name':default_contact_id.id,
                    'body': _('Invoice No.'),
                    'media_url':media_url
                }
            whatsapp = twilio_base.create(data)
            if whatsapp:
                whatsapp.sendwhtsp()
                                  
        
class TwilioBase(models.Model):
    _inherit = 'twiliosms.base'

    @api.multi
    def send(self,nro_from,nro_to):
        context = self._context
        if context.get('default_res_id') and context.get('whatsapp_sent'):
            invoice = self.env['account.invoice'].browse(context['default_res_id'])
            #invoice = invoice.with_context(mail_post_autofollow=True)
            attachment_ids=self.env.context.get('attachment_ids')
            invoice.sent = True
            invoice.message_post(body=_("Invoice sent via whatsapp"),attachment_ids=attachment_ids)
        return super(TwilioBase, self).send(nro_from,nro_to)        
