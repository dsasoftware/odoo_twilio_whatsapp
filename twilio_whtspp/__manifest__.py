{
    'name': "Twilio Whatsapp",
    'summary': """ This App has the ability to send Whatsapp using twilio """,
    'version': '10.0.1.0.0',
    'category': 'Whatsapp app',
    'website': "dsasoftware.com.ve",
    'author': "DSA Software SG, C.A.",
    'license': 'AGPL-3',
    'installable': True,
    'application': False,
    'images': ['images/main_screenshot.png'],
    'depends': ['base','account'],
    'data': [
         'security/ir.model.access.csv',
         'views/twiliowhatsapp_send_view.xml',
         'views/res_config_view.xml',
         'views/res_partner_view.xml',
         'views/account_invoice_view.xml',
         'views/mail_message_view.xml',
         'wizard/account_invoice_whatsapp_view.xml',
         'wizard/partner_whatsapp_view.xml',
         'wizard/account_invoice_send_views.xml'
    ],
}
