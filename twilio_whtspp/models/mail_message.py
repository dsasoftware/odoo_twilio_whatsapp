# -*- coding: utf-8 -*-
from odoo import models, api, fields, _


class MailChanel(models.Model):
    _inherit = 'mail.channel'
    
    whatsapp_send = fields.Boolean('Send by Whatsapp',help="", default=False)

class MailMessage(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self,values):
        result = super(MailMessage,self).create(values)
        if result:
            if result.model in ['sale.order','purchase.order','mail.channel'] and result.message_type in ['comment']:
                if result.model=='mail.channel' and not self.env['mail.channel'].browse([result.res_id]).whatsapp_send:
                    return  result
                #model_obj = self.env[result.model ].search([('name','=',result.record_name)])[0]
                if result.model=='mail.channel':
                    follow_obj = self.env['mail.channel.partner'].search([('channel_id','=',result.res_id),('partner_id','!=',False)])                    
                else:
                    follow_obj = self.env['mail.followers'].search([('res_model','=',result.model),('res_id','=',result.res_id),('partner_id','!=',False)])
                follow_ids = [x.partner_id.id for x in follow_obj] 
                partner = self.env['res.partner'].browse(follow_ids) 
                attachment = False
                media_url = False
                if result.attachment_ids:  
                    attachment = [x.id for x in result.attachment_ids]
                    config_obj = self.env['ir.config_parameter'].get_param('web.base.url')
                    media_url = config_obj + "/web/attachments/token/"+self.env['ir.attachment'].browse(attachment[0]).access_token                                        
                msg = result.body.replace('<p>','')
                msg = msg.replace('</p>','')
                for rec in partner:
                    rec.send_message_whatsapp(msg, media_url=media_url, attachment=attachment)
        return result
                