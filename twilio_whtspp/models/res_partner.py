from odoo import models, fields, api
from odoo.exceptions import Warning
#import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    mobile_whatsapp = fields.Char(string="Mobile Whatsapp", size=14)
    enabled_whatsapp = fields.Boolean('Permite Whatsapp',help="", default=False)
    
    @api.multi
    def sms_action(self):
        self.ensure_one()
        for record in self:
            if record.mobile_whatsapp and record.enabled_whatsapp:
                conf = self.env['ir.config_parameter']
                sandbox = conf.get_param('twilio_whtspp.twilio_sandbox', True)
                if sandbox:                
                    default_mobile = conf.get_param('twilio_whtspp.twilio_test_numeber', False)
                else:
                    default_mobile = conf.get_param('twilio_whtspp.twilio_account_numeber', False)
                default_contact_id = self.env['contact.twiliocontact_base'].search([('to','=',record.mobile_whatsapp)])
                if default_contact_id:
                    default_contact_id = default_contact_id[0]
                else:
                    data = {'to':self.mobile_whatsapp,'contact_name_contactclass':self.name,'image':self.image}
                    default_contact_id = self.env['contact.twiliocontact_base'].create(data)
                if default_mobile:        
                    return {
                        'name': 'Whatsapp Compose',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'twiliosms.base',
                        'target': 'new',
                        'type': 'ir.actions.act_window',
                        'context': {'default_from_': default_mobile,'default_to':self.mobile_whatsapp,'default_contact_name':default_contact_id.id}
                     }
                else:
                    raise Warning("You must configure the number for whatsapp")
            else:
                raise Warning("Partner has no whatsapp number or does not support messages")
        
    @api.multi
    def write(self, values):
        result = super(ResPartner,self).write(values)
        if result and self.mobile_whatsapp:
            data = {'to':self.mobile_whatsapp,'contact_name_contactclass':self.name,'image':self.image}
            contact_id = self.env['contact.twiliocontact_base'].search([('to','=',self.mobile_whatsapp)])
            if contact_id:
                contact_id.write(data)
            else:
                self.env['contact.twiliocontact_base'].create(data)
                
        return result
    
    @api.model
    def send_message_whatsapp(self,message,attachment=False, media_url=False):  
        template = self.env.ref('account.email_template_edi_invoice', False)  
        twilio_base = self.env['twiliosms.base']
        twilio_contact = self.env['contact.twiliocontact_base']
        if self.mobile_whatsapp and self.enabled_whatsapp:
            default_contact_id = twilio_contact.search([('to','=',self.mobile_whatsapp)])
            print('default_contact_id:',default_contact_id)
            default_contact_id = default_contact_id and default_contact_id[0] or False
            if not default_contact_id:  
                data = {
                        'contact_name_contactclass':self.name,
                        'to':self.mobile_whatsapp,
                        'image':self.image                     
                    }
                default_contact_id = twilio_contact.create(data)
            data = {
                    'contact_name':default_contact_id.id,
                    'body': message,
                }
            if media_url:
                data.update({'media_url':media_url})
            print('data partner:',data)
            whatsapp = twilio_base.create(data)
            if whatsapp:
                whatsapp.sendwhtsp()    