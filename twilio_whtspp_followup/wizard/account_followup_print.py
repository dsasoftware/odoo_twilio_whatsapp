# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime
import time

from odoo import api, fields, models, _
from odoo import tools

class AccountFollowupPrint(models.TransientModel):
    _inherit = 'account_followup.print'

    def process_partners(self, partner_ids, data):
        result0 = super(AccountFollowupPrint,self).process_partners(partner_ids, data)
        partner_obj = self.env['res.partner']
        partner_ids_to_print = []
        nbmanuals = 0
        manuals = {}
        nbmails = 0
        nbunknownmails = 0
        nbprints = 0
        resulttext = ''
        for partner in self.env['account_followup.stat.by.partner'].browse(
                partner_ids):
            if partner.max_followup_id.send_whatsapp:
                nbunknownmails += partner.partner_id.do_partner_whatsapp()
                nbmails += 1
        if nbunknownmails == 0:
            resulttext += str(nbmails) + _(" whatsapp(s) sent")
        else:
            resulttext += str(nbmails) + _(
                " whatsapp(s) should have been sent, but ") + str(
                nbunknownmails) + _(
                " had unknown email address(es)") + "\n <BR/> "

        resulttext += "<br/>"+result0['resulttext']
        result = {}
        result['needprinting'] = result0['needprinting']
        result['resulttext'] = resulttext
        result['action'] = result0['action'] 
        return result


