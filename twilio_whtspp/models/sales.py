# -*- coding: utf-8 -*-
from odoo import models, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.multi
    @api.returns('self', lambda value: value.id)
    def message_post(self, body='', subject=None, message_type='notification',
             subtype=None, parent_id=False, attachments=None,
             content_subtype='html', **kwargs):
        return super(SaleOrder,self).message_post(body,subject,message_type,subtype,parent_id,attachments,content_subtype, **kwargs)