
##################################    TWILIO PYTHON   ###########################################

from twilio import *
from odoo import models, fields, api
from twilio.rest import TwilioRestClient
from odoo.exceptions import Warning
#import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
#from twilio import TwilioRestException

import requests

def url_ok(url):
    page = requests.get(url)
    return page.status_code == 200

class contact_contact_base(models.Model):
    _name = 'contact.twiliocontact_base'
    _rec_name = 'contact_name_contactclass'

    contact_name_contactclass = fields.Char(string="Contact Name")
    to = fields.Char(string="To", size=14)
    image = fields.Binary(string="Contact Avatar")

class TwilioBase(models.Model):
    _name = 'twiliosms.base'
    _inherits = {'contact.twiliocontact_base':'contact_name'}

    _defaults = {
              'date_time': lambda *a:datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
              }
    from_ = fields.Char(string="Your Number", default='+14155238886',required=True,help="Pls, provide your twilio number")
    contact_name = fields.Many2one('contact.twiliocontact_base', string="Contact Name", required=True,help="Your contact number here")
    body = fields.Char(string="Message", size=160)
    display_all = fields.Text(string="Sender,Receiver,Message", readonly=True)
    date_time = fields.Datetime(string="Date & Time",  readonly=True)
    msg_status = fields.Char(string="Status")
    media_url = fields.Char(string="Media URL")

    @api.onchange('msg_status')
    def onchangedate(self):
        self.date_time = str(datetime.now())

    @api.multi
    @api.depends('from_', 'to', 'body')
    @api.model
    def sendsms(self):
        return self.send(self.from_,self.to)

    @api.multi
    @api.depends('from_', 'to', 'body')
    @api.model
    def sendwhtsp(self):
        return self.send('whatsapp:%s'%self.from_,'whatsapp:%s'%self.to)
    
    @api.multi
    @api.depends('from_', 'to', 'body')
    @api.model
    def send(self,nro_from,nro_to, media=False):
        conf = self.env['ir.config_parameter']
        sandbox = conf.get_param('twilio_whtspp.twilio_sandbox', True)
        if sandbox:                
            default_account_sid = conf.get_param('twilio_whtspp.twilio_test_account_sid', False)
            default_auth_tocken = conf.get_param('twilio_whtspp.twilio_test_auth_tocken', False)            
            default_mobile = conf.get_param('twilio_whtspp.twilio_test_numeber', False)
            if not default_mobile:
                raise Warning("sandbox number is empty")
            if not default_account_sid:
                raise Warning("Twilio sandbox account is empty")
            if not default_auth_tocken:
                raise Warning("Twilio sandbox tocken is empty")
        else:
            default_account_sid = conf.get_param('twilio_whtspp.twilio_account_sid', False)
            default_auth_tocken = conf.get_param('twilio_whtspp.twilio_auth_tocken', False)            
            default_mobile = conf.get_param('twilio_whtspp.twilio_account_numeber', False)
            if not default_mobile:
                raise Warning("sandbox number is empty")    
            if not default_mobile:
                raise Warning("sandbox number is empty")
            if not default_account_sid:
                raise Warning("Twilio account is empty")
            if not default_auth_tocken:
                raise Warning("Twilio tocken is empty")
        if not self.body:
            raise Warning("Message part is empty")
        else:
            try:
                client = TwilioRestClient(account=default_account_sid, token=default_auth_tocken)
                media_url = self.media_url
                if media:
                    media_url = media            
                if media_url:
                    client.messages.create(from_=nro_from, to=nro_to, body=self.body, MediaUrl=media_url)
                else:
                    client.messages.create(from_=nro_from, to=nro_to, body=self.body)
                self.display_all = "Sender Number  " + str(self.from_) + "  Receiver Number " + str(self.to) \
                + "Message Content -> " + str(self.body) + "  at " + str(self.date_time)
                msg_status = str("Message Sent")
                self.msg_status = msg_status

            except Warning as e:
                print(e)
                domain=[('contact_name_contactclass', '=', self.contact_name_contactclass)]
                res = self.env['contact.twiliocontact_base'].search(domain)
                count_domain = [('contact_name_contactclass', '=', self.contact_name_contactclass)]
                rec = self.env['contact.twiliocontact_base'].search_count(count_domain)
                raise Warning("This is not a registered Number, plz verify (OR) register")
        return    
    
    @api.multi
    @api.depends('from_', 'to')
    @api.model
    def callnumber(self):
        conf = self.env['ir.config_parameter']
        sandbox = conf.get_param('twilio_whtspp.twilio_sandbox', True)
        if sandbox:                
            default_account_sid = conf.get_param('twilio_whtspp.twilio_test_account_sid', False)
            default_auth_tocken = conf.get_param('twilio_whtspp.twilio_test_auth_tocken', False)            
            default_mobile = conf.get_param('twilio_whtspp.twilio_test_numeber', False)
            if not default_mobile:
                raise Warning("sandbox number is empty")
            if not default_account_sid:
                raise Warning("Twilio sandbox account is empty")
            if not default_auth_tocken:
                raise Warning("Twilio sandbox tocken is empty")
        else:
            default_account_sid = conf.get_param('twilio_whtspp.twilio_account_sid', False)
            default_auth_tocken = conf.get_param('twilio_whtspp.twilio_auth_tocken', False)            
            default_mobile = conf.get_param('twilio_whtspp.twilio_account_numeber', False)
            if not default_mobile:
                raise Warning("sandbox number is empty")    
            if not default_mobile:
                raise Warning("sandbox number is empty")
            if not default_account_sid:
                raise Warning("Twilio account is empty")
            if not default_auth_tocken:
                raise Warning("Twilio tocken is empty")        
        if not self.to:
            raise Warning("contact number is empty")
        else:
            try:
                client = TwilioRestClient(account='ACe1287d3a54cec629bbe010beb9caecd6', token='76e84104afb6bf922bdb5c68354844da')
                client.calls.create(url="http://demo.twilio.com/docs/voice.xml", to=self.to, from_=self.from_)
            except Warning as e:
                print(e)
                raise Warning("This is not a registered Number, plz verify OR register")
        return
    
class MailMail(models.Model):
    _inherit = 'mail.mail'    

    twilio_whatsapp = fields.Boolean(string='Es un whatsapp', default=False)
    
    ""
    @api.multi
    def send_get_whatsapp_dict(self, partner=None):
        """Return a dictionary for specific email values, depending on a
        partner, or generic to the whole recipients given by mail.email_to.

            :param Model partner: specific recipient partner
        """
        self.ensure_one()
        body = self.send_get_mail_body(partner=partner)
        body_alternative = tools.html2plaintext(body)
        res = {
            'body': body,
            'body_alternative': body_alternative,
            'email_to': self.send_get_mail_to(partner=partner),
        }
        return res    
    
    @api.multi
    def send_get_mail_to(self, partner=None):
        self.ensure_one()
        if partner and self.twilio_whatsapp:
            email_to = [formataddr((partner.name or 'False', partner.mobile_whatsapp or 'False'))]
            return email_to            
        return super(MailMail, self).send_get_mail_to(partner)
    
    @api.model
    def create(self, values):
        context = self._context
        if context.get('whatsapp_sent'):
            values.append({'twilio_whatsapp':True})
        return super(MailMail, self).create(values)
    
    @api.multi
    def send2(self, auto_commit=False, raise_exception=False):
        if not self.twilio_whatsapp:
            return super(MailMail, self).send2(auto_commit, raise_exception)
        tw_base = self.env['twiliosms.base']
        data = send_get_whatsapp_dict()
        whtsp = tw_base.create(data)
        whtsp.sendwhtsp()             
