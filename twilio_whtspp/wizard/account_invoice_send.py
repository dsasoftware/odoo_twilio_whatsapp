# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError


class AccountInvoiceSend(models.TransientModel):
    _inherit = 'account.invoice.send'

    is_whatsapp = fields.Boolean('Whatsapp', default=False)

    @api.multi
    def _send_whatsapp(self):
        if self.is_whatsapp:
            self.mapped('invoice_ids').send_invoice_whatsapp()
            if self.env.context.get('mark_invoice_as_sent'):
                self.mapped('invoice_ids').write({'sent': True})

    @api.multi
    def send_and_print_action(self):
        self.ensure_one()
        self._send_whatsapp()        
        res = super(AccountInvoiceSend,self).send_and_print_action()
        return {'type': 'ir.actions.act_window_close'}
